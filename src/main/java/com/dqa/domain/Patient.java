package com.dqa.domain;

import java.util.List;

/**
 * Created by wgandy on 9/14/19.
 */
public class Patient {
    public static String INDEX = "patient";
    private String id;
    private String firstName;
    private String lastName;
    private List<Regime> regimes;
    private List<String> emails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Regime> getRegimes() {
        return regimes;
    }

    public void setRegimes(List<Regime> regimes) {
        this.regimes = regimes;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
}
