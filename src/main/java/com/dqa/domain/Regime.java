package com.dqa.domain;

/**
 * Created by wgandy on 9/14/19.
 */
public class Regime {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
