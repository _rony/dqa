package com.dqa.service;

import com.dqa.domain.Patient;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by wgandy on 9/14/19.
 */
@Service
public class PatientService {
    private RestHighLevelClient client;
    private ObjectMapper objectMapper;


    @Autowired
    public PatientService(RestHighLevelClient client, ObjectMapper objectMapper) {
        this.client = client;
        this.objectMapper = objectMapper;
    }

    public String createPatient(Patient document) throws Exception {

        if(document.getId() == null){
            throw new IllegalArgumentException("Patient must have an id");
        }

        Map<String, Object> documentMapper = objectMapper.convertValue(document, Map.class);

        IndexRequest indexRequest = new IndexRequest(Patient.INDEX)
                .source(documentMapper);

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

        return indexResponse
                .getResult()
                .name();
    }
}
