package com.dqa.web.rest;

import com.dqa.domain.Patient;
import com.dqa.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wgandy on 9/14/19.
 */
@RestController
@RequestMapping("/api/v1/patient")
public class PatientController {

    private PatientService service;

    @Autowired
    public PatientController(PatientService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity createPatient(@RequestBody Patient document) throws Exception {
        return new ResponseEntity<>(service.createPatient(document), HttpStatus.CREATED);
    }
}
